package co.killion.plugintemplate

import co.aikar.commands.BukkitCommandManager
import org.bukkit.plugin.java.JavaPlugin

lateinit var instance: PluginTemplate
lateinit var commandManager: BukkitCommandManager

class PluginTemplate : JavaPlugin() {

    override fun onEnable() {
        instance = this
        commandManager = BukkitCommandManager(this)
    }

    override fun onDisable() {

    }

    override fun onLoad() {
        saveDefaultConfig()
    }

}