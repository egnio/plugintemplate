package co.killion.plugintemplate.extensions

import co.killion.plugintemplate.instance
import com.okkero.skedule.BukkitSchedulerController
import com.okkero.skedule.CoroutineTask
import com.okkero.skedule.SynchronizationContext
import com.okkero.skedule.schedule
import org.bukkit.Bukkit

// Technically not an extension function, but we'll put it here for the sake of organization...
fun asyncTask(block: suspend BukkitSchedulerController.() -> Unit): CoroutineTask {
    return Bukkit.getScheduler().schedule(instance, SynchronizationContext.ASYNC, block)
}